<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pilihan Pagi</title>
	<link rel="stylesheet" type="text/css" media="all" href="assets/css/animate.min.css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
  </head>
  <body>
  	
  	<!-- FACEBOOK SHARE -->
  	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=749341348498363";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

	<!-- TWITTER SHARE -->
	<script>
	window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
	</script>

	<!-- GOOGLE PLUS SHARE -->
	<script src="https://apis.google.com/js/platform.js" async defer></script>

	<!-- HEADER NAVIGATION -->
    <div class="header" style="display: none;">
    	<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#"><img src="assets/img/pilihanpagiheader.png"></a>
		    </div>

		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#"><h4><strong>Ceritakan ke teman:</strong></h4></a></li>
		        <li><a href="#"><div class="fb-share-button" data-href="http://pilihanpagi.com/"></div></a></li>
		        <li><br><a href="https://twitter.com/share" class="twitter-share-button"
					  data-dnt="true"
					  data-count="none"
					  data-via="mcdonalds_id">
					
					</a></li>
		        <li><a href="#"><div class="g-plus" data-action="share" data-annotation="none" data-height="24" data-href="http://pilihanpagi.com/"></div></a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
    </div>

    <div class="container-fluid">
    	<!-- VIDEO BACKGROUND -->
	    <iframe id="video-bg" src="//www.youtube.com/embed/EalIcoMP0kQ?rel=0&amp;controls=0&amp;showinfo=0;autoplay=0&cc_load_policy=1" frameborder="0" allowfullscreen></iframe>

		<div id="layerone" class="layerone animated">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div id="text">
						<div class="images text-center">
						<img src="assets/img/pilihanpagi.png">
						</div>
						<h3>
							Setiap pilihan yang dibuat di pagi hari, mengubah setiap menit dalam hidupmu.<br>
							Lihat bagaimana PAGIMU TENTUKAN HARIMU.<br>
							Tonton Videonya dan temukan link tersembunyi untuk mendapatkan voucher gratis Breakfast Wrap.<br><br>
							Mulai
						</h3>
						<br><br>
					</div>
					<div class="imgbtn text-center">
						<img id="btnhide" src="assets/img/btnmulai.png">
					</div>
			
				</div>
			</div>
		</div>
	</div>

	<script language="javascript">
	    $(function() {
	        $("#btnhide").click(function() {
	            animate(".layerone", 'slideOutUp');
	            $(".header").css("display", "block");
	            $(".header").show(3000);
	            animate(".header", 'fadeInDownBig');
	        });
	    });
	     
	    function animate(element_ID, animation) {
	        $(element_ID).addClass(animation);
	        
	    }
	</script>
   
  	
  </body>
</html>